const EleventyFetch = require('@11ty/eleventy-fetch');

module.exports = async function() {
	const query = `
		*[_type == "headshot"]{
			...,
			image{
				asset->
			}
		}
	`;

	const { result } = await EleventyFetch(
		`https://enlfzusn.api.sanity.io/v2023-02-11/data/query/production?query=${query}`,
		{
			duration: '1h',
			type: 'json',
			fetchOptions: {}
		}
	);

	return result;
};

