const EleventyFetch = require('@11ty/eleventy-fetch');

module.exports = async function() {
	const query = `
		*[_type == "link"]{
			...,
			image{
				asset->
			}
		}
	`;

	const { result } = await EleventyFetch(
		`https://enlfzusn.api.sanity.io/v2023-02-11/data/query/production?query=${query}`,
		{
			duration: '1h',
			type: 'json',
			fetchOptions: {}
		}
	);


	const links = result.map( async (r) => {

		if ( 'string' === typeof r.image ) {
			return r;
		}

		const imageRequest = await EleventyFetch( r.image.asset.url, {
			duration: '1h',
			type: 'text',
			fetchOptions: {}
		} );

		return Object.assign( r, { image: imageRequest} );
	} );

	return await Promise.all( links );
};
