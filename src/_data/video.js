const EleventyFetch = require('@11ty/eleventy-fetch');

module.exports = async function() {
	const query = `
		*[_type == "video"]{
			...,
			poster{
				asset->
			},
			sources[]{
				asset->
			}
		}[0]
	`;
	const { result } = await EleventyFetch(
		`https://enlfzusn.api.sanity.io/v2023-02-11/data/query/production?query=${query}`,
		{
			duration: '1h',
			type: 'json',
			fetchOptions: {}
		}
	);
	return {
		title: result.title,
		poster: result.poster.asset.url,
		width: result.dimensions?.width || result.poster.asset.metadata.dimensions.width,
		height: result.dimensions?.height || result.poster.asset.metadata.dimensions.height,
		sources: result.sources.map( ({ asset }) => ({
			src: asset.url,
			type: asset.mimeType
		}))
	};
};

