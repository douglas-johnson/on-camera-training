const EleventyFetch = require('@11ty/eleventy-fetch');
const {toHTML} = require('@portabletext/to-html')

module.exports = async function() {
	const { result } = await EleventyFetch( 'https://enlfzusn.api.sanity.io/v2023-02-11/data/query/production?query=*[_type == "class"]' , {
		duration: '1h',
		type: 'json',
		fetchOptions: {}
	});
	return result.map( ( c ) => {
		return {
			...c,
			description: toHTML( c.description ),
		}
	} );
}
