/**
 * Eleventy Configuration
 *
 * @link https://www.11ty.dev/docs/config/
 */

const pluginWebc = require('@11ty/eleventy-plugin-webc');
const bundlerPlugin = require('@11ty/eleventy-plugin-bundle');

/**
 * Configure Eleventy
 *
 * @param {UserConfig} eleventyConfig
 * @return {Object}
 */
function configureEleventy(eleventyConfig) {

	/**
	 * Plugins
	 */
	eleventyConfig.addPlugin(bundlerPlugin);
	eleventyConfig.addPlugin(pluginWebc, {components: './src/_components/**/*.webc'});

	/**
	 * Pass Through CSS
	 *
	 * @link https://www.11ty.dev/docs/copy/
	 */
	eleventyConfig.addPassthroughCopy({
		'src/_includes/assets/css': 'assets/css',
		'src/apple-touch-icon.png': 'apple-touch-icon.png',
		'src/favicon-192x192.png': 'favicon-192x192.png',
		'src/favicon-512x512.png': 'favicon-512x512.png',
		'src/favicon.ico': 'favicon.ico',
		'src/favicon.svg': 'favicon.svg',
		'src/manifest.webmanifest': 'manifest.webmanifest',
		'src/robots.txt': 'robots.txt',
		'src/_redirects': '_redirects'
	});

	eleventyConfig.addJavaScriptFunction( "getSanitySrcSet", getSanitySrcSet );

	return {
		htmlTemplateEngine: "webc",
		dir: {
			input: 'src',
			output: 'dist'
		}
	}
}

/**
 * Get Sanity Src Set
 * 
 * @param {Object} image
 * @param {Number} levels
 * @return {String}
 */
function getSanitySrcSet( image, levels ) {
	return Array
		.from( Array(levels).keys() )
		.map( getWidthFunction( image.metadata.dimensions.width ) )
		.map( ( width ) => {
			return `${image.url}?w=${width} ${width}w`;
		} )
		.join(', ');
}

/**
 * Get Width Function
 * 
 * @param {Number} width
 * @return {Function}
 */
function getWidthFunction( width ) {
	return ( value ) => {
		return Math.round( width / (value + 1) );
	}
}	

module.exports = configureEleventy;
